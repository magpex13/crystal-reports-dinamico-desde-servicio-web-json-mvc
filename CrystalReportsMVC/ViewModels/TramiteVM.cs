﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace CrystalReportsMVC.ViewModels
{
    public class TramiteVM
    {
        public string codSecuencia{ get; set; }
        public string fecRecibe { get; set; }
        public string fecRemitente { get; set; }
        public string TxtAreaRecibe { get; set; }
        public string TxtAreaRemite { get; set; }
        public string TxtDocumento { get; set; }
        public string TxtObservacionRecibe { get; set; }
        public string TxtObservacionRemite { get; set; }

        public TramiteVM(string json)
        {
            JObject jsonObject = JObject.Parse(json);
            JToken jUser = jsonObject["TramiteDetalleResult"];
            var aux =jUser.Count();
            codSecuencia = (string)jUser[0]["proCodSecuencia"];
            fecRecibe = (string)jUser[0]["proFecRecibe"];
            fecRemitente = (string)jUser[0]["proFecRemite"];
            TxtAreaRecibe = (string)jUser[0]["proTxtAreaRecibe"];
            TxtAreaRemite = (string)jUser[0]["proTxtAreaRemite"];
            TxtDocumento = (string)jUser[0]["proTxtDocumento"];
            TxtObservacionRecibe = (string)jUser[0]["proTxtObservacionRecibe"];
            TxtObservacionRemite = (string)jUser[0]["proTxtObservacionRemite"];
           
        }
    }
}