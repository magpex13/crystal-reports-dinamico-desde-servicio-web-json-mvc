﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace CrystalReportsMVC.Utils
{
    public class JsonParserCrack:IDisposable
    {
        public string titulo { get; set; }
        public string JSON { get; set; }
        public string urlJson { get; set; }
        private char caracterPorDefecto;

        public JsonParserCrack()
        {
            caracterPorDefecto = '/';
        }

        public Dictionary<string, List<string>> parsearDataJSONdesdeURL(string url)
        {
            try
            {

                urlJson = url;
                JSON = limpiarJson(GET(url));
                var json = JSON.ToCharArray();

                //Se parsean las columnas y las filas en un arreglo
                List<string> lstParseado = new List<string>();
                char caracter;
                string cadena;

                for (int i = 0; i < json.Length; i++)
                {
                    caracter = Convert.ToChar(json[i]);
                    if (caracter != caracterPorDefecto)
                    {
                        if (esCaracterEspecial(caracter))
                        {
                            json[i] = caracterPorDefecto;
                        }
                        else
                        {
                            if (caracter != ',')
                            {
                                if (caracter == Convert.ToChar('"'))
                                {
                                    cadena = dameCadena(json, i);
                                    if (cadena != null)
                                        lstParseado.Add(cadena);
                                }
                                else
                                    lstParseado.Add(dameCadenaDecimal(json, i));
                            }
                            else
                                json[i] = caracterPorDefecto;
                        }
                    }

                }

                titulo = lstParseado[0];

                //Se pasan los datos del arreglo generado a un diccionario
                Dictionary<string, List<string>> diccionario = new Dictionary<string, List<string>>();
                for (int i = 0; i < lstParseado.Count; i++)
                {
                    if (i != 0)
                    {
                        if (i % 2 != 0)
                        {
                            if (diccionario.ContainsKey(lstParseado[i]))
                            {
                                diccionario[lstParseado[i]].Add(lstParseado[i + 1]);
                            }
                            else
                            {
                                diccionario.Add(lstParseado[i], new List<string>());
                                diccionario[lstParseado[i]].Add(lstParseado[i + 1]);
                            }
                        }
                    }

                }

                return diccionario;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string dameCadenaDecimal(char[] json, int indiceDato)
        {
            string cadena = "";
            char caracter;

            for (int i = indiceDato; i < json.Length; i++)
            {
                caracter = Convert.ToChar(json[i]);
                if (esCaracterEspecial(caracter))
                {
                    json[i] = caracterPorDefecto;
                    return cadena;
                }

                if (caracter == ',')
                {
                    json[i] = caracterPorDefecto;
                    return cadena;
                }

                if (caracter != caracterPorDefecto)
                {
                    json[i] = caracterPorDefecto;
                    cadena += caracter;
                }
            }

            return cadena;
        }

        private string dameCadena(char[] json, int indiceDato)
        {
            string cadena = "";
            char caracter;

            json[indiceDato] = caracterPorDefecto;

            for (int i = indiceDato; i < json.Length; i++)
            {
                caracter = Convert.ToChar(json[i]);

                if (esCaracterEspecial(caracter))
                {
                    json[i] = caracterPorDefecto;
                    return null;
                }

                if (caracter == '"')
                {
                    json[i] = caracterPorDefecto;
                    return cadena;
                }

                if (caracter != caracterPorDefecto)
                {
                    cadena += caracter;
                    json[i] = caracterPorDefecto;
                }
            }


            return cadena;

        }

        private bool esCaracterEspecial(char caracter)
        {
            if (caracter == Convert.ToChar('\\') || caracter == Convert.ToChar('{') || caracter == Convert.ToChar('}') || caracter == Convert.ToChar('[') || caracter == Convert.ToChar(']') || caracter == Convert.ToChar(':'))
            {
                return true;
            }

            return false;
        }

        public string limpiarJson(string json)
        {
            var aux = json.ToCharArray();
            string cadena = "";

            foreach (var caracter in aux)
            {
                if (caracter != Convert.ToChar('\\') && (int)caracter != 39)
                    cadena += caracter;
            }

            return cadena;
        }

        private string GET(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }

        public void Dispose()
        {
            
        }
    }
}