﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CrystalReportsMVC.Utils;
using CrystalReportsMVC.Content;
using CrystalReportsMVC.Models;
using CrystalDecisions.CrystalReports.Engine;
using CrystalReportsMVC.ViewModels;
using System.Data;
using System.IO;
using System.Net;

namespace CrystalReportsMVC.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            WebClient cliente = new WebClient();

            string json = cliente.DownloadString("http://www.munisanisidro.gob.pe/Servicios/TramiteDocumentario.svc/json/tramitedetalle/0000574748");

            TramiteVM tramite = new TramiteVM(json);

            return View();
        }

        public ActionResult GenerarReporte(IndexVM objIndexVM)
        {
            ReporteDinamico objReportDoc = new ReporteDinamico();

            DataSetGenerico ds = new DataSetGenerico();

            using(JsonParserCrack crack = new JsonParserCrack())
            {
                try
                {
                    //var dic = crack.parsearDataJSONdesdeURL("http://www.munisanisidro.gob.pe/Servicios/TramiteDocumentario.svc/json/tramitedetalle/0000574748");
                    //var dic = crack.parsearDataJSONdesdeURL("http://www.munisanisidro.gob.pe/Servicios/Usuario.svc/json/login/2/0/0/08225169");
                     //var dic = crack.parsearDataJSONdesdeURL("http://www.munisanisidro.gob.pe/Servicios/Tupa.svc/json/derechopago/007/006");
                    //var dic = crack.parsearDataJSONdesdeURL(" http://www.munisanisidro.gob.pe/Servicios/TramiteDocumentario.svc/json/tramitedetalle/0000574748");
                    //var dic = crack.parsearDataJSONdesdeURL("http://www.munisanisidro.gob.pe/Servicios/Usuario.svc/json/login/1/01068013/S4O3G0/0");
                    var dic = crack.parsearDataJSONdesdeURL(objIndexVM.URL);

                    if (dic == null)
                        return RedirectToAction("Index", "Home");

                    var colNames = dic.Keys.ToList();

                    //Se setean los nombres de las columanas del CustomersReport.rpt
                    List<TextObject> headerReporte = objReportDoc.ReportDefinition.Sections["Section1"].ReportObjects.OfType<TextObject>().ToList();
                    List<TextObject> lstTextObjects = objReportDoc.ReportDefinition.Sections["Section2"].ReportObjects.OfType<TextObject>().ToList();

                    headerReporte[0].Text += " " + crack.titulo;

                    for (int i = 0; i < lstTextObjects.Count; i++)
                    {
                        lstTextObjects[i].ObjectFormat.EnableCanGrow = true;

                        if (colNames.Count > i)
                            lstTextObjects[i].Text = colNames[i];
                        else
                            lstTextObjects[i].Text = string.Empty;
                    }

                    //Se llenan los datos
                    DataRow dr;
                    var valores = dic[colNames[0]];

                    for (int i = 0; i < valores.Count; i++)
                    {
                        dr = ds.Tables[0].Rows.Add();
                        for (int j = 0; j < colNames.Count; j++)
                        {
                            dr[j] = dic[colNames[j]][i];
                        }
                    }

                }
                catch (Exception ex)
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            objReportDoc.SetDataSource(ds);

            Stream stream = objReportDoc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(stream, "application/pdf");
        }
	}
}